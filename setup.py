from gateway_mailgun import __api_level__, __version__

import setuptools


with open('README.md', 'r') as f:
	long_description = f.read()

with open('./requirements.txt', 'r') as f:
	requirements = f.readlines()

setuptools.setup(
	name='gateway_mailgun',
	version=__version__,
	author='Mahmoud Abduljawad',
	author_email='mahmoud@masaar.com',
	description='Nawah Gateway for Mailgun',
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/nawah_io/gateway_mailgun',
	package_data={
		'gateway_mailgun': ['requirements.txt'],
	},
	packages=[
		'gateway_mailgun',
	],
	project_urls={
		'Docs: Gitlab': 'https://gitlab.com/nawah_io/gateway_mailgun',
		'Gitlab: issues': 'https://gitlab.com/nawah_io/gateway_mailgun/-/issues',
		'Gitlab: repo': 'https://gitlab.com/nawah_io/gateway_mailgun',
	},
	classifiers=[
		'Programming Language :: Python :: 3',
		'Programming Language :: Python :: 3.8',
		'Development Status :: 5 - Production/Stable',
		'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
		'Operating System :: OS Independent',
		'Topic :: Internet :: WWW/HTTP',
		'Framework :: AsyncIO',
	],
	python_requires='>=3.8',
	install_requires=requirements,
)
